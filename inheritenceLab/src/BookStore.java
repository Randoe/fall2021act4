public class BookStore {
    public static void main(String[]args){

        Book[] books = new Book[5];
        books[0] = new Book("thebee movie", "Jerry seinfeld");
        books[2] = new Book("scooby doo and the legend of the phantasaur", "jk simmons");
        books[1] = new ElectronicBook("Cyberchase", "nasR", 1);
        books[3] = new ElectronicBook("your mother", "Julien DPH", 645);
        books[4] = new ElectronicBook("fast10 your seatbelts", "nicola cage", 78);

        for(Book b: books){
            System.out.println(b);
        }
        
    }
}
