public class ElectronicBook extends Book{
    private int numberBytes;

    public ElectronicBook(String title, String author, int numberBytes){
        super(title, author);
        this.numberBytes = numberBytes;
    }

    public int getNumberBytes(){
        return this.numberBytes;
    }

    public String toString(){
        return "title: "+this.title+" author: "+this.getAuthor()+" size: "+this.numberBytes;
        //or do
        //return super.toString()+" size: "+this.numberBytes;
    }
}